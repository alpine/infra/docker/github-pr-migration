FROM alpine:edge

# Environment variable GITHUB_ACCESS_TOKEN and
# GITHUB_WEBHOOK_SECRET must be set using -e cmdflag.

RUN apk -X http://dl-cdn.alpinelinux.org/alpine/edge/testing add --no-cache py3-six noprs

RUN install -o noprs -g noprs -d /var/lib/noprs/
COPY --chown=noprs:noprs comment.md /var/lib/noprs/

EXPOSE 8080

USER noprs
ENTRYPOINT [ "noprs", "-a", "", "-p", "8080", "/var/lib/noprs/comment.md" ]
