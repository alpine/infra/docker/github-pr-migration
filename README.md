# github-pr-migration

Dockerfile for [noprs][noprs github]

## Usage

Build an image using:

	$ docker build -t noprs .

Setup a [github webhook][github webhook], create an API
[access token][github token] and run the image using:

	$ docker run -p 127.0.0.1:80:8080/tcp \
		-e GITHUB_ACCESS_TOKEN="<GitHub API Access Token>" \
		-e GITHUB_WEBHOOK_SECRET="<GitHub Webhook Secret>" noprs

[noprs github]: https://github.com/nmeum/noprs
[github webhook]: https://developer.github.com/webhooks/
[github token]: https://help.github.com/en/github/authenticating-to-github/creating-a-personal-access-token-for-the-command-line
