Thanks for your pull request!

Unfortunately, Alpine is currently in the process of migrating from
GitHub to [GitLab][alpine gitlab]. This means that the Alpine [GitHub
organization][alpine github] will only be used as a read-only mirror.
Patches have to be submitted using one of the following methods:

* As a GitLab merge request on the Alpine [GitLab instance][alpine gitlab].
  You can also use your GitHub account to sign-up for an account.
* If you don't want to register a GitLab account you can create
  patches using `git-format-patch(1)` and submit them to the
  mailing list. Further instructions on this procedure can be
  found on the [wiki][alpine wiki patches].

Sorry for the inconvenience, but we don't have the capacity to review
patches on three different platforms. If you are interested in further
information on this topic you can consult [the mailinglist][ml github].

[alpine gitlab]: https://gitlab.alpinelinux.org/
[alpine github]: https://github.com/alpinelinux
[alpine wiki patches]: https://wiki.alpinelinux.org/wiki/Creating_patches
[ml github]: https://lists.alpinelinux.org/~alpine/devel/%3C3OHWOOASIXH2T.20R4V2YVU7P8Y%408pit.net%3E
